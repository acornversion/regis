import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'


Vue.use(VueRouter)
var firebaseConfig = {
  apiKey: "AIzaSyCAb3YHVRhO2OoSYetdAqAIvHdoxhbAz1A",
  authDomain: "testregis-b124a.firebaseapp.com",
  databaseURL: "https://testregis-b124a.firebaseio.com",
  projectId: "testregis-b124a",
  storageBucket: "testregis-b124a.appspot.com",
  messagingSenderId: "688088529650",
  appId: "1:688088529650:web:2e2d3bdc8eea3a4ee9ee6b"
};
firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();
export const auth = firebase.auth()
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */




export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}
