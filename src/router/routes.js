const routes = [
  { path: '/', component: () => import('pages/login.vue') },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/register', component: () => import('pages/register.vue'),
        name: 'register'
      },
      {
        path: '/list', component: () => import('pages/list.vue'),
        name: 'list'
      },
      {
        path: '/admin', component: () => import('pages/admin.vue'),
        name: 'admin'
      },

    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
